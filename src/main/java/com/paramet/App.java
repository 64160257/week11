package com.paramet;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Carter");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        bat1.walk();
        bat1.run();

        Fish fish1 = new Fish("Moo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("Hawk", "Hawk Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile cro1 = new Crocodile("Lham");
        cro1.eat();
        cro1.sleep();
        cro1.swim();
        cro1.crawl();

        Bird bird1 = new Bird("Jib Jib");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        Snake snk1 = new Snake("Bok");
        snk1.crawl();
        snk1.eat();
        snk1.sleep();

        Submarine sub1 = new Submarine("Too", "Too V8");
        sub1.swim();

        Cat cat1 = new Cat("Chi");
        cat1.walk();
        cat1.run();
        cat1.eat();
        cat1.sleep();

        Dog dog1 = new Dog("Pun");
        dog1.walk();
        dog1.run();
        dog1.eat();
        dog1.sleep();

        Human hu1 = new Human("Mink");
        hu1.walk();
        hu1.run();
        hu1.eat();
        hu1.sleep();

        Rat rat1 = new Rat("Jid Rid");
        rat1.walk();
        rat1.run();
        rat1.eat();
        rat1.sleep();

        System.out.println();

        Flyable[] flyObjects = {bat1,bird1,plane1};
        for(int i=0; i<flyObjects.length;i++) {
            flyObjects[i].takeoff();
            flyObjects[i].fly();
            flyObjects[i].landing();
        }

        System.out.println();    

        Walkable[] walkObjects = {cat1,dog1,hu1,rat1};
        for(int i=0; i<walkObjects.length;i++) {
            walkObjects[i].walk();
            walkObjects[i].run();
        }

        System.out.println();
        
        Swimable[] swimObjects = {sub1,fish1,cro1};
        for(int i=0; i<swimObjects.length;i++) {
            swimObjects[i].swim();
        }

        System.out.println();

        Crawlable[] crawlObjects = {snk1,cro1};
        for(int i=0; i<crawlObjects.length;i++) {
            crawlObjects[i].crawl();
        }
    }   

}
