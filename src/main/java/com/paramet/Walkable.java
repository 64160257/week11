package com.paramet;

public interface Walkable {
    public void walk();
    public void run();
}
