package com.paramet;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}
